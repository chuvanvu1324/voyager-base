@extends('voyager::master')

@section('css')

@include('voyager::compass.includes.styles')

@stop

@section('page_header')
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">{{ __('voyager::generic.compass') }}</h3>
            {{-- <span class="page-description">{{ __('voyager::compass.welcome') }}</span> --}}
        </div>
    </div>
</div>
@stop

@section('content')
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @include('voyager::alerts')
    <div class="kt-portlet kt-portlet--mobiless">
        <div class="kt-portlet__body">
            <div class="page-content compass container-fluid">
                <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-primary" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link @if(empty($active_tab) || (isset($active_tab) && $active_tab=='resources' )){!! "
                            active" !!}@endif" data-toggle="tab" href="#kt_tabs_res" role="tab">
                            <i class="flaticon-book"></i> {{ __('voyager::compass.resources.title') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if($active_tab=='commands' ){!! " active" !!}@endif" data-toggle="tab"
                            href="#kt_tabs_cmd" role="tab">
                            <i class="la la-terminal"></i> {{ __('voyager::compass.commands.title') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if($active_tab=='logs' ){!! " active" !!}@endif" data-toggle="tab"
                            href="#kt_tabs_logs" role="tab">
                            <i class="fa fa-puzzle-piece"></i> {{ __('voyager::compass.logs.title') }}
                        </a>
                    </li>

                </ul>
                <div class="tab-content">

                    <div class="tab-pane {{$active_tab}} @if(empty($active_tab) || (isset($active_tab) && $active_tab=='resources' )) {{'active'}} @endif"
                        id="kt_tabs_res" role="tabpanel">
                        <div id="resources">
                            <h3>
                                <i class="voyager-book"></i> {{ __('voyager::compass.resources.title') }}
                                <small>{{ __('voyager::compass.resources.text') }}</small>
                            </h3>

                            <div class="collapsible">
                                <div class="collapse-head" data-toggle="collapse" data-target="#links"
                                    aria-expanded="true" aria-controls="links">
                                    <h4>{{ __('voyager::compass.links.title') }}</h4>
                                    <i class="la la-angle-down" style="display: none;"></i>
                                    <i class="la la-angle-up"></i>
                                </div>
                                <div class="collapse-content collapse show" id="links">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="https://docs.laravelvoyager.com" target="_blank"
                                                class="voyager-link"
                                                style="background-image:url('{{ voyager_asset('images/compass/documentation.jpg') }}')">
                                                <span class="resource_label"><i class="voyager-documentation"></i> <span
                                                        class="copy">{{ __('voyager::compass.links.documentation') }}</span></span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="https://laravelvoyager.com" target="_blank" class="voyager-link"
                                                style="background-image:url('{{ voyager_asset('images/compass/voyager-home.jpg') }}')">
                                                <span class="resource_label"><i class="voyager-browser"></i> <span
                                                        class="copy">{{ __('voyager::compass.links.voyager_homepage') }}</span></span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="https://larapack.io" target="_blank" class="voyager-link"
                                                style="background-image:url('{{ voyager_asset('images/compass/hooks.jpg') }}')">
                                                <span class="resource_label"><i class="voyager-hook"></i> <span
                                                        class="copy">{{ __('voyager::compass.links.voyager_hooks') }}</span></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="collapsible">
                                <div class="collapse-head" data-toggle="collapse" data-target="#flaticon"
                                    aria-expanded="true" aria-controls="fonts">
                                    <h4>Flaticon</h4>
                                    <i class="la la-angle-down" style="display: none;"></i>
                                    <i class="la la-angle-up"></i>
                                </div>
                                <div class="collapse-content collapse show" id="flaticon">
                                    @include('voyager::compass.includes.fonts.flaticon')
                                </div>
                            </div>
                            
                            <div class="collapsible">
                                <div class="collapse-head" data-toggle="collapse" data-target="#fontawesome"
                                    aria-expanded="true" aria-controls="fonts">
                                    <h4>Fontawesome 5</h4>
                                    <i class="la la-angle-down" style="display: none;"></i>
                                    <i class="la la-angle-up"></i>
                                </div>
                                <div class="collapse-content collapse show" id="fontawesome">
                                    @include('voyager::compass.includes.fonts.fontawesome')
                                </div>
                            </div>

                            <div class="collapsible">
                                <div class="collapse-head" data-toggle="collapse" data-target="#lineawesome"
                                    aria-expanded="true" aria-controls="fonts">
                                    <h4>Lineawesome</h4>
                                    <i class="la la-angle-down" style="display: none;"></i>
                                    <i class="la la-angle-up"></i>
                                </div>
                                <div class="collapse-content collapse show" id="lineawesome">
                                    @include('voyager::compass.includes.fonts.lineawesome')
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="tab-pane @if($active_tab == 'commands'){!! 'active' !!}@endif" id="kt_tabs_cmd"
                        role="tabpanel">
                        <div id="commands">
                            <h3><i class="voyager-terminal"></i> {{ __('voyager::compass.commands.title') }}
                                <small>{{ __('voyager::compass.commands.text') }}</small></h3>
                            <div id="command_lists">
                                @include('voyager::compass.includes.commands')
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane @if($active_tab == 'logs'){!! 'active' !!}@endif" id="kt_tabs_logs"
                        role="tabpanel">
                        <div id="logs">
                            <div class="row">

                                @include('voyager::compass.includes.logs')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script>
    $('document').ready(function(){
        $('.collapse-head').click(function(){
            var collapseContainer = $(this).parent();
            if(collapseContainer.find('.collapse-content').hasClass('show')){
                collapseContainer.find('.la-angle-up').fadeOut('fast');
                collapseContainer.find('.la-angle-down').fadeIn('slow');
            } else {
                collapseContainer.find('.la-angle-down').fadeOut('fast');
                collapseContainer.find('.la-angle-up').fadeIn('slow');
            }
        });
    });
</script>
<!-- JS for commands -->
<script>
    $(document).ready(function(){
            $('.command').click(function(){
                $(this).find('.cmd_form').slideDown();
                $(this).addClass('more_args');
                $(this).find('input[type="text"]').focus();
            });

            $('.close-output').click(function(){
                $('#commands pre').slideUp();
            });
        });

</script>

<!-- JS for logs -->
<script>
    $(document).ready(function () {
        $('.table-container tr').on('click', function () {
          $('#' + $(this).data('display')).toggle();
        });
        $('#table-log').DataTable({
          "order": [1, 'desc'],
          "stateSave": true,
          "language": {!! json_encode(__('voyager::datatable')) !!},
          "stateSaveCallback": function (settings, data) {
            window.localStorage.setItem("datatable", JSON.stringify(data));
          },
          "stateLoadCallback": function (settings) {
            var data = JSON.parse(window.localStorage.getItem("datatable"));
            if (data) data.start = 0;
            return data;
          }
        });

        $('#delete-log, #delete-all-log').click(function () {
          return confirm('{{ __('voyager::generic.are_you_sure') }}');
        });
      });
</script>
@stop