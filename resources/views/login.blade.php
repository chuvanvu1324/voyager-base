@extends('voyager::auth.master')

@section('content')
<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
            style="background-image: url({{ metoger_asset('media/bg/bg-1.jpg') }});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="{{ metoger_asset('media/logos/logo-mini-2-md.png') }}">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">{{ __('voyager::login.signin_below') }}</h3>
                        </div>
                        <form class="kt-form" action="{{ route('voyager.login') }}" method="POST">
                            @if(!$errors->isEmpty())
                            @foreach($errors->all() as $err)
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <div class="alert-text">{{ $err }}</div>
                                <div class="alert-close">
                                    <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>
                                </div>
                            </div>
                            @endforeach
                            @endif
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" name="email" id="email" value="{{ old('email') }}"
                                    placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                            </div>
                            <div class="input-group">
                                <input type="password" name="password"
                                    placeholder="{{ __('voyager::generic.password') }}" class="form-control" required>
                            </div>
                            <div class="row kt-login__extra">
                                <div class="col">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="remember" id="remember" value="1">
                                        {{ __('voyager::generic.remember_me') }}
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col kt-align-right">
                                    <a href="javascript:;" id="kt_login_forgot" class="kt-link kt-login__link">Forget
                                        Password ?</a>
                                </div>
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit"
                                    class="btn btn-pill kt-login__btn-primary">{{ __('voyager::generic.login') }}</button>
                            </div>
                        </form>
                    </div>
                    <div class="kt-login__forgot">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Forgotten Password ?</h3>
                            <div class="kt-login__desc">Enter your email to reset your password:</div>
                        </div>
                        <form class="kt-form" action="">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email"
                                    autocomplete="off">
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_forgot_submit"
                                    class="btn btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                                <button id="kt_login_forgot_cancel"
                                    class="btn btn-pill kt-login__btn-secondary">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
@endsection
