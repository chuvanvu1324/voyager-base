@php
$nav_items = config('voyager.dashboard.navbar_items');
$segments = array_filter(explode('/', str_replace(route('voyager.dashboard'), '', Request::url())));
$url = route('voyager.dashboard');
@endphp

<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
            <div class="kt-subheader__breadcrumbs">
                <a class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                @if (count($segments) == 0)
                    <span
                        class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">{{ __('voyager::generic.dashboard') }}</span>
                @else
                    <a href="{{ route('voyager.dashboard') }}"
                        class="kt-subheader__breadcrumbs-link">{{ __('voyager::generic.dashboard') }}</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    @foreach ($segments as $segment)
                        @php
                            $url .= '/' . $segment;
                        @endphp
                        @if ($loop->last)
                            <span
                                class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">{{ ucfirst(urldecode($segment)) }}</span>
                        @else
                            <a href="{{ $url }}"
                                class="kt-subheader__breadcrumbs-link">{{ ucfirst(urldecode($segment)) }}</a>
                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        @endif
                    @endforeach

                @endif
            </div>
        </div>
    </div>

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">
        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                <div class="kt-header__topbar-user">
                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                    <span class="kt-header__topbar-username kt-hidden-mobile">{{ Auth::user()->name }}</span>
                    @if ($user_avatar)
                        <img alt="Pic" src="{{ $user_avatar }}" />
                    @else
                        <span
                            class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ strtoupper(substr(Auth::user()->name, 0, 1)) }}</span>
                    @endif
                </div>
            </div>
            <div
                class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                <!--begin: Head -->
                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                    style="background-image: url({{ metoger_asset('media/misc/bg-1.jpg') }})">
                    <div class="kt-user-card__avatar">
                        @if ($user_avatar)
                            <img alt="Pic" src="{{ $user_avatar }}" />
                        @else
                            <span
                                class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ strtoupper(substr(Auth::user()->name, 0, 1)) }}</span>
                        @endif
                    </div>
                    <div class="kt-user-card__name">
                        <span>{{ Auth::user()->name }}</span>
                        <small>{{ Auth::user()->email }}</small>
                    </div>
                </div>

                <!--end: Head -->

                <!--begin: Navigation -->
                <div class="kt-notification">
                    @if (is_array($nav_items) && !empty($nav_items))
                        @foreach ($nav_items as $name => $item)
                            <a href="{{ isset($item['route']) && Route::has($item['route']) ? route($item['route']) : (isset($item['route']) ? $item['route'] : '#') }}"
                                {!! isset($item['target_blank']) && $item['target_blank'] ? 'target="_blank"' : '' !!}
                                class="kt-notification__item {!!  isset($item['classes']) && !empty($item['classes']) ? $item['classes'] : '' !!}">
                                @if (isset($item['icon_class']) && !empty($item['icon_class']))
                                    <div class="kt-notification__item-icon">
                                        <i class="{!!  $item['icon_class'] !!}"></i>
                                    </div>
                                @endif
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        {{ __($name) }}
                                    </div>

                                </div>
                            </a>
                        @endforeach
                    @endif
                    <div class="kt-notification__custom kt-space-between">
                        <form action="{{ route('voyager.logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</button>
                        </form>
                    </div>
                </div>

                <!--end: Navigation -->
            </div>
        </div>

        <!--end: User Bar -->
    </div>

    <!-- end:: Header Topbar -->
</div>
