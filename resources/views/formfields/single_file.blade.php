@if(isset($dataTypeContent->{$row->field}))
<div data-field-name="{{ $row->field }}">
  <a class="fileType" target="_blank"
    href="{{ Storage::disk(config('voyager.storage.disk'))->url($dataTypeContent->{$row->field}) }}"
    data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}">>
    Download
  </a>
  <a href="#" class="voyager-x remove-single-file"></a>
</div>
@endif
<div class="custom-file">
  <input class="custom-file-input" @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" name="{{ $row->field }}[]" multiple="multiple">
  <label class="custom-file-label" for="customFile">Choose file</label>
</div>