@push('css')
    <link rel="stylesheet" href="{{ metoger_asset('plugins/summernote/summernote.css') }}">
    <style>
        .note-editor.note-frame.fullscreen .note-editable {
            background-color: #fff;
        }
    </style>
@endpush

<textarea class="summernote" name="{{ $row->field }}">
    {{ old($row->field, $dataTypeContent->{$row->field} ?? '') }}
</textarea>
@push('javascript')
    <script>
        $(document).ready(function() {
             $('.summernote').summernote({
                height: 600
            });
        });
    </script>
@endpush
