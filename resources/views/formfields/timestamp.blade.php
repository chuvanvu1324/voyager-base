<div class="input-group date">
       <input type="text" class="form-control" placeholder="" id="kt_datetimepicker_4_2" @if($row->required == 1) required @endif type="datetime" name="{{ $row->field }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('m/d/Y g:i A') }}@else{{old($row->field)}}@endif"/>
       <div class="input-group-append">
              <span class="input-group-text">
                     <i class="la la-check-circle-o glyphicon-th"></i>
              </span>
       </div>
</div>

@push('javascript')
       <script src="{{ metoger_asset('js/pages/crud/forms/widgets/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
@endpush
