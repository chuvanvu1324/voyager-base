@extends('voyager::master')
@section('pre_css')
    <style>
        .kt-widget19 {
            margin: 10px;
        }

        .kt-widget19__labels a {
            cursor: pointer !important;
        }

        .icon-widget {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .icon-widget i {
            font-size: 48px;
            background: rgba(0, 0, 0, .3);
            border-radius: 50%;
            width: 100px;
            height: 100px;
            display: block;
            margin: 0 auto;
            color: #eee;
            line-height: 100px;
            text-align: center;
        }

    </style>
@stop
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        {{-- @include('voyager::dimmers') --}}
        <div class="kt-portlet-custom">
            <div class="kt-portlet__body  kt-portlet__body--fit">
                <div class="row row-col-separator-lg">
                    @include('voyager::dimmers')
                </div>
            </div>
        </div>
    </div>
@stop
