@php
$dimmerGroups = Voyager::dimmers();
@endphp

@if (count($dimmerGroups))
@foreach($dimmerGroups as $dimmerGroup)
    @php
    $count = $dimmerGroup->count();
    $classes = [
        'col-md-12',
        'col-lg-'.($count >= 2 ? '6' : '12'),
        'col-xl-'.($count >= 3 ? '4' : ($count >= 2 ? '6' : '12')),
    ];
    $class = implode(' ', $classes);
    $prefix = "<div class='{$class}'>";
    $surfix = '</div>';
    @endphp
    @if ($dimmerGroup->any())
        {!! $prefix.$dimmerGroup->setSeparator($surfix.$prefix)->display().$surfix !!}
    @endif
@endforeach
@endif
