<button type="button" class="btn btn-danger btn-elevate-hover btn-pill" id="bulk_delete_btn">
    <i class="flaticon-delete"></i> {{ __('voyager::generic.bulk_delete') }}
</button>&nbsp;

{{-- Bulk delete modal --}}
<!--begin::Modal-->
<div class="modal fade" id="bulk_delete_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content panel-warning">
            <div class="modal-header">
                <h5 class="modal-title">
                    <i class="voyager-trash"></i> {{ __('voyager::generic.are_you_sure_delete') }} <span
                        id="bulk_delete_count"></span> <span id="bulk_delete_display_name"></span>?
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
                <form action="{{ route('voyager.' . $dataType->slug . '.index') }}/0" id="bulk_delete_form" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_delete_input" value="">
                    <input type="submit" class="btn btn-danger pull-right delete-confirm"
                        value="{{ __('voyager::generic.bulk_delete_confirm') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_plural')) }}">
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->

{{-- <script>
    window.onload = function() {
        // Bulk delete selectors
        var $bulkDeleteBtn = $('#bulk_delete_btn');
        var $bulkDeleteModal = $('#bulk_delete_modal');
        var $bulkDeleteCount = $('#bulk_delete_count');
        var $bulkDeleteDisplayName = $('#bulk_delete_display_name');
        var $bulkDeleteInput = $('#bulk_delete_input');
        // Reposition modal to prevent z-index issues
        $bulkDeleteModal.appendTo('body');
        // Bulk delete listener
        $bulkDeleteBtn.click(function() {
            var ids = [];
            var $checkedBoxes = $('#kt_dataTable input[type=checkbox]:checked').not('.select_all');
            var count = $checkedBoxes.length;
            
            if (count) {
                // Reset input value
                $bulkDeleteInput.val('');
                // Deletion info
                var displayName = count > 1 ?
                    '{{ $dataType->getTranslatedAttribute('display_name_plural') }}' :
                    '{{ $dataType->getTranslatedAttribute('display_name_singular') }}';
                displayName = displayName.toLowerCase();
                $bulkDeleteCount.html(count);
                $bulkDeleteDisplayName.html(displayName);
                // Gather IDs
                $.each($checkedBoxes, function() {
                    var value = $(this).val();
                    ids.push(value);
                })
                console.log(`ids`, ids);
                // Set input value
                $bulkDeleteInput.val(ids);
                // Show modal
                $bulkDeleteModal.modal('show');
            } else {
                // No row selected
                toastr.warning('{{ __('voyager::generic.bulk_delete_nothing') }}');
            }
        });
    }

</script> --}}