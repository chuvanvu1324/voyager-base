@php
$segments = array_filter(explode('/', str_replace(route('voyager.dashboard'), '', Request::url())));
$url = route('voyager.dashboard');
@endphp
@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing') . ' ' . $dataType->getTranslatedAttribute('display_name_plural'))

@section('css')
    <link href="{{ metoger_asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ metoger_asset('css/pages/bread/browse.css') }}" rel="stylesheet" type="text/css" />
    <style>
        #search-input {
            padding: 1px;
            /* border: 1px solid #eaeaf1; */
            /* border-radius: 6px; */
            background-color: #fff;
            margin-bottom: 20px;
            display: -webkit-box;
            display: flex;
        }

        #search-input > div {
            flex: 1;
            padding: 0;
        }

        #search-input > div > span.select2 {
            width: 100% !important;
        }
        
        #search-input > div > span.input-group-btn {
            position: absolute;
            right: 0;
            top: 0;
        }


        
        #search-input .input-group {
            /* display: -webkit-box;
            display: flex;
            position: relative; */
        }
    </style>
@stop

@section('page_header')
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">{{ $dataType->getTranslatedAttribute('display_name_plural') }}</h3>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    @can('add', app($dataType->model_name))
                        <a href="{{ route('voyager.' . $dataType->slug . '.create') }}">
                            <button type="button" class="btn btn-success btn-elevate-hover btn-pill">
                                <i class="flaticon-plus"></i> {{ __('voyager::generic.add_new') }}
                            </button>&nbsp;
                        </a>
                    @endcan
                    @can('delete', app($dataType->model_name))
                        @include('voyager::partials.bulk-delete')
                    @endcan
                    @can('edit', app($dataType->model_name))
                        @if (isset($dataType->order_column) && isset($dataType->order_display_column))
                            <a href="{{ route('voyager.' . $dataType->slug . '.order') }}">
                                <button type="button" class="btn btn-primary btn-elevate-hover btn-pill">
                                    <i class="flaticon-list"></i> {{ __('voyager::bread.order') }}
                                </button>&nbsp;
                            </a>
                        @endif
                    @endcan
                    @can('delete', app($dataType->model_name))
                        @if ($usesSoftDeletes)
                            <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle"
                                data-on="{{ __('voyager::bread.soft_deletes_off') }}"
                                data-off="{{ __('voyager::bread.soft_deletes_on') }}">
                        @endif
                    @endcan
                    @foreach ($actions as $action)
                        @if (method_exists($action, 'massAction'))
                            @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
                        @endif
                    @endforeach
                    @include('voyager::multilingual.language-selector')
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('voyager::alerts')
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                @if ($isServerSide)
                    <form method="get" class="form-search">
                        <div id="search-input">
                            <div class="col-2">
                                <select id="search_key" name="key">
                                    @foreach($searchNames as $key => $name)
                                        <option value="{{ $key }}" @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2">
                                <select id="filter" name="filter">
                                    <option value="contains" @if($search->filter == "contains") selected @endif>contains</option>
                                    <option value="equals" @if($search->filter == "equals") selected @endif>=</option>
                                </select>
                            </div>
                            <div class="input-group col-md-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="flaticon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (Request::has('sort_order') && Request::has('order_by'))
                            <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
                            <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
                        @endif
                    </form>
                @endif
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_dataTable">
                    <thead>
                        <tr>
                            @if ($showCheckboxColumn)
                                <th class="dt-not-orderable">
                                    <input type="checkbox" class="select_all">
                                </th>
                            @endif
                            @foreach ($dataType->browseRows as $row)
                                <th>
                                    @if ($isServerSide && $row->type !== 'relationship')
                                        <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}">
                                    @endif
                                    {{ $row->getTranslatedAttribute('display_name') }}
                                    @if ($isServerSide)
                                        @if ($row->isCurrentSortField($orderBy))
                                            @if ($sortOrder == 'asc')
                                                <i class="voyager-angle-up pull-right"></i>
                                            @else
                                                <i class="voyager-angle-down pull-right"></i>
                                            @endif
                                        @endif
                                        </a>
                                    @endif
                                </th>
                            @endforeach
                            <th class="actions text-right dt-not-orderable">{{ __('voyager::generic.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dataTypeContent as $data)
                            <tr>
                                @if ($showCheckboxColumn)
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}"
                                            value="{{ $data->getKey() }}">
                                    </td>
                                @endif
                                @foreach ($dataType->browseRows as $row)
                                    @php
                                        if ($data->{$row->field . '_browse'}) {
                                            $data->{$row->field} = $data->{$row->field . '_browse'};
                                        }
                                    @endphp
                                    <td>
                                        @if (isset($row->details->view))
                                            @include($row->details->view, ['row' => $row, 'dataType' => $dataType,
                                            'dataTypeContent' => $dataTypeContent, 'content' => $data->{$row->field},
                                            'action' => 'browse', 'view' => 'browse', 'options' => $row->details])
                                        @elseif($row->type == 'image')
                                            <img src="@if (!filter_var($data->{$row->field},
                                            FILTER_VALIDATE_URL)) {{ Voyager::image($data->{$row->field}) }}@else{{ $data->{$row->field} }} @endif" style="width:100px">
                                        @elseif($row->type == 'relationship')
                                            @include('voyager::formfields.relationship', ['view' => 'browse','options' =>
                                            $row->details])
                                        @elseif($row->type == 'select_multiple')
                                            @if (property_exists($row->details, 'relationship'))

                                                @foreach ($data->{$row->field} as $item)
                                                    {{ $item->{$row->field} }}
                                                @endforeach

                                            @elseif(property_exists($row->details, 'options'))
                                                @if (!empty(json_decode($data->{$row->field})))
                                                    @foreach (json_decode($data->{$row->field}) as $item)
                                                        @if (@$row->details->options->{$item})
                                                            {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                        @endif
                                                    @endforeach
                                                @else
                                                    {{ __('voyager::generic.none') }}
                                                @endif
                                            @endif

                                        @elseif($row->type == 'multiple_checkbox' && property_exists($row->details,
                                            'options'))
                                            @if (@count(json_decode($data->{$row->field})) > 0)
                                                @foreach (json_decode($data->{$row->field}) as $item)
                                                    @if (@$row->details->options->{$item})
                                                        {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                    @endif
                                                @endforeach
                                            @else
                                                {{ __('voyager::generic.none') }}
                                            @endif

                                        @elseif(($row->type == 'select_dropdown' || $row->type == 'radio_btn') &&
                                            property_exists($row->details, 'options'))

                                            {!! $row->details->options->{$data->{$row->field}} ?? '' !!}

                                        @elseif($row->type == 'date' || $row->type == 'timestamp')
                                            @if (property_exists($row->details, 'format') && !is_null($data->{$row->field}))
                                                {{ \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) }}
                                            @else
                                                {{ $data->{$row->field} }}
                                            @endif
                                        @elseif($row->type == 'checkbox')
                                            @if (property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                @if ($data->{$row->field})
                                                    <span class="label label-info">{{ $row->details->on }}</span>
                                                @else
                                                    <span class="label label-primary">{{ $row->details->off }}</span>
                                                @endif
                                            @else
                                                {{ $data->{$row->field} }}
                                            @endif
                                        @elseif($row->type == 'color')
                                            <span class="badge badge-lg"
                                                style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                        @elseif($row->type == 'text')
                                            @include('voyager::multilingual.input-hidden-bread-browse')
                                            <div>
                                                {{ mb_strlen($data->{$row->field}) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}
                                            </div>
                                        @elseif($row->type == 'text_area')
                                            @include('voyager::multilingual.input-hidden-bread-browse')
                                            <div>
                                                {{ mb_strlen($data->{$row->field}) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}
                                            </div>
                                        @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                            @include('voyager::multilingual.input-hidden-bread-browse')
                                            @if (json_decode($data->{$row->field}) !== null)
                                                @foreach (json_decode($data->{$row->field}) as $file)
                                                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
                                                        target="_blank">
                                                        {{ $file->original_name ?: '' }}
                                                    </a>
                                                    <br />
                                                @endforeach
                                            @else
                                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}"
                                                    target="_blank">
                                                    Download
                                                </a>
                                            @endif
                                        @elseif($row->type == 'rich_text_box')
                                            @include('voyager::multilingual.input-hidden-bread-browse')
                                            <div>
                                                {{ mb_strlen(strip_tags($data->{$row->field}, '<b><i><u>')) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}
                                            </div>
                                        @elseif($row->type == 'coordinates')
                                            @include('voyager::partials.coordinates-static-image')
                                        @elseif($row->type == 'multiple_images')
                                            @php $images = json_decode($data->{$row->field}); @endphp
                                            @if ($images)
                                                @php $images = array_slice($images, 0, 3); @endphp
                                                @foreach ($images as $image)
                                                    <img src="@if (!filter_var($image,
                                                        FILTER_VALIDATE_URL)) {{ Voyager::image($image) }}@else{{ $image }} @endif" style="width:50px">
                                                @endforeach
                                            @endif
                                        @elseif($row->type == 'media_picker')
                                            @php
                                                if (is_array($data->{$row->field})) {
                                                    $files = $data->{$row->field};
                                                } else {
                                                    $files = json_decode($data->{$row->field});
                                                }
                                            @endphp
                                            @if ($files)
                                                @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                    @foreach (array_slice($files, 0, 3) as $file)
                                                        <img src="@if (!filter_var($file,
                                                            FILTER_VALIDATE_URL)) {{ Voyager::image($file) }}@else{{ $file }} @endif" style="width:50px">
                                                    @endforeach
                                                @else
                                                    <ul>
                                                        @foreach (array_slice($files, 0, 3) as $file)
                                                            <li>{{ $file }}</li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                                @if (count($files) > 3)
                                                    {{ __('voyager::media.files_more', ['count' => count($files) - 3]) }}
                                                @endif
                                            @elseif (is_array($files) && count($files) == 0)
                                                {{ trans_choice('voyager::media.files', 0) }}
                                            @elseif ($data->{$row->field} != '')
                                                @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                    <img src="@if (!filter_var($data->{$row->field},
                                                    FILTER_VALIDATE_URL)) {{ Voyager::image($data->{$row->field}) }}@else{{ $data->{$row->field} }} @endif" style="width:50px">
                                                @else
                                                    {{ $data->{$row->field} }}
                                                @endif
                                            @else
                                                {{ trans_choice('voyager::media.files', 0) }}
                                            @endif
                                        @else
                                            @include('voyager::multilingual.input-hidden-bread-browse')
                                            <span>{{ $data->{$row->field} }}</span>
                                        @endif
                                    </td>
                                @endforeach
                                <td class="no-sort no-click bread-actions">
                                    @foreach ($actions as $action)
                                        @if (!method_exists($action, 'massAction'))
                                            @include('voyager::bread.partials.actions', ['action' => $action])
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                
                @if ($isServerSide)
                <div>
                    <div class="pull-left">
                        <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                            'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                'from' => $dataTypeContent->firstItem(),
                                'to' => $dataTypeContent->lastItem(),
                                'all' => $dataTypeContent->total()
                            ]) }}</div>
                    </div>
                    <div class="pull-right">
                        {{ $dataTypeContent->appends([
                            's' => $search->value,
                            'filter' => $search->filter,
                            'key' => $search->key,
                            'order_by' => $orderBy,
                            'sort_order' => $sortOrder,
                            'showSoftDeleted' => $showSoftDeleted,
                        ])->links() }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <!--begin::Modal-->
    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content panel-warning">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}
                        {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h5>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right"
                        data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit"
                            class="btn btn-danger pull-right delete-confirm">{{ __('voyager::generic.delete_confirm') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--end::Modal-->
@stop

@section('javascript')
    <script src="{{ metoger_asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            @if (!$dataType->server_side)
                var table = $('#kt_dataTable').DataTable({!! json_encode(
                    array_merge([
                        "responsive" => true,
                        "order" => $orderColumn,
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [
                            ['targets' => 'dt-not-orderable', 'searchable' =>  false, 'orderable' => false],
                        ],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#kt_dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', '__id') }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        @if($usesSoftDeletes)
            @php
                $params = [
                    's' => $search->value,
                    'filter' => $search->filter,
                    'key' => $search->key,
                    'order_by' => $orderBy,
                    'sort_order' => $sortOrder,
                ];
            @endphp
            $(function() {
                $('#show_soft_deletes').change(function() {
                    if ($(this).prop('checked')) {
                        $('#kt_dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                    }else{
                        $('#kt_dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                    }

                    $('#redir')[0].click();
                })
            })
        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>
    <script>
        window.onload = function() {
            // Bulk delete selectors
            var $bulkDeleteBtn = $('#bulk_delete_btn');
            var $bulkDeleteModal = $('#bulk_delete_modal');
            var $bulkDeleteCount = $('#bulk_delete_count');
            var $bulkDeleteDisplayName = $('#bulk_delete_display_name');
            var $bulkDeleteInput = $('#bulk_delete_input');
            // Reposition modal to prevent z-index issues
            $bulkDeleteModal.appendTo('body');
            // Bulk delete listener
            $bulkDeleteBtn.click(function() {
                var ids = [];
                var $checkedBoxes = $('#kt_dataTable input[type=checkbox]:checked').not('.select_all');
                var count = $checkedBoxes.length;
                
                if (count) {
                    // Reset input value
                    $bulkDeleteInput.val('');
                    // Deletion info
                    var displayName = count > 1 ?
                        '{{ $dataType->getTranslatedAttribute('display_name_plural') }}' :
                        '{{ $dataType->getTranslatedAttribute('display_name_singular') }}';
                    displayName = displayName.toLowerCase();
                    $bulkDeleteCount.html(count);
                    $bulkDeleteDisplayName.html(displayName);
                    // Gather IDs
                    $.each($checkedBoxes, function() {
                        var value = $(this).val();
                        ids.push(value);
                    })
                    console.log(`ids`, ids);
                    // Set input value
                    $bulkDeleteInput.val(ids);
                    // Show modal
                    $bulkDeleteModal.modal('show');
                } else {
                    // No row selected
                    toastr.warning('{{ __('voyager::generic.bulk_delete_nothing') }}');
                }
            });
        }

    </script>
    
@stop
