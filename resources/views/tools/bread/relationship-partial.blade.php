<style>
.row-dd-relationship {
    border-left: 3px solid #F7325B;
}

.row-dd {
    border-bottom: 1px solid #eee;
    margin: 0 -7px;
    padding-top: 20px;
    padding-bottom: 0;
}


.row-dd-relationship i.voyager-heart {
    font-size: 16px;
    position: relative;
    padding: 4px 5px 5px 4px;
    background: #fff;
    border-radius: 50px;
    color: #F7325B;
    border: 2px solid #F2435C;
    width: 28px;
    height: 28px;
    text-align: center;
    display: block;
    float: left;
    top: -4px;
    left: -4px;
}

.row-dd-relationship .voyager-relationship-details-btn {
    color: #F7325B;
    border: 1px solid #fff;
    border-radius: 25px;
    padding: 10px 15px 10px 30px;
    font-weight: 400;
    font-size: 12px;
    float: right;
    position: relative;
    background: #f9f9fb;
    transition: all .2s ease;
    cursor: pointer;
}

.row-dd-relationship .voyager-relationship-details-btn i {
    top: 50%;
    font-size: 12px;
    position: absolute;
    left: 10px;
    transform: translateY(-50%);
}

.row-dd-relationship .voyager-relationship-details-btn span.close_text, 
.row-dd-relationship .voyager-relationship-details-btn.open i.voyager-angle-down {
    display: none;
}
.row-dd-relationship .voyager-relationship-details-btn i.voyager-angle-up {
    display: none;
}
.row-dd-relationship .voyager-relationship-details-btn.open i.voyager-angle-up {
    display: block;
}


.row-dd-relationship .voyager-relationship-details-btn.open span.open_text {
    display: none;
}
.row-dd-relationship .voyager-relationship-details-btn span.open_text {
    display: inline;
}
.row-dd-relationship .voyager-relationship-details-btn.open span.close_text {
    display: inline;
}

.row-dd-relationship .voyager-relationship-details {
    display: block;
    background: #f5f5f7;
    margin-bottom: 0;
    height: auto;
    padding: 15px 15px 45px;
    border-top: 1px solid #efeff2;
}

.row-dd-relationship .delete_relationship {
    position: absolute;
    right: 0;
    bottom: 0;
    padding: 6px 28px 6px 10px;
    background: red;
    border-top-left-radius: 3px;
    border-bottom-left-radius: 0;
    color: #fff;
    font-size: 10px;
    font-weight: 700;
    cursor: pointer;
    transition: opacity .1s linear;
    opacity: .7;
    z-index: 2;
}

.row-dd-relationship .relationship_details_content {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}

.row-dd-relationship .relationship_details_content p {
    float: left;
    padding: 6px 20px;
    background: #fff;
    line-height: 22px;
    margin-right: 0;
    border: 1px solid #eaeaea;
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    color: #333;
    margin-bottom: 0;
    border-right: 0;
}

.row-dd-relationship .relationship_details_content p.relationship_table_select {
    background: #EFEFEF;
    color: #76838f;
    font-weight: 700;
    font-size: 12px;
    padding-top: 8px;
}

.row-dd-relationship .relationship_details_content .select2 {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    /* border: 1px solid #eaeaea; */
    border-right: 0;
    margin-right: 0;
    height: 38px;
}

.row-dd-relationship .relationship_details_content select {
    /* border: 1px solid #f1f1f1; */
    margin-left: 10px;
}



.row-dd-relationship .relationship_details_content .select2 {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    /* border: 1px solid #eaeaea; */
    border-right: 0;
    margin-right: 0;
    height: 38px;
    display: block;
    z-index: 1!important;
}
.row-dd.row-dd-relationship > .voyager-relationship-details > div:nth-child(2) > span:nth-child(6) {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: inline-block;
}

.row-dd-relationship .relationship_details_content .relationship_type+span {
    font-weight: 700;
}
.voyager-relationship-details .form-control {
    border-radius: 0;
    height: 38px;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}

.row-dd-relationship .relationship_details_content.belongsTo, .row-dd-relationship .relationship_details_content.hasOneMany {
    width: 100%;
    display: none;
}

.row-dd-relationship .relationship_details_content.belongsTo.flexed, .row-dd-relationship .relationship_details_content.hasOneMany.flexed {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}
.landing-page .app-content-b.contact-us form, .row-dd-relationship .relationship_details_content.margin_top {
    margin-top: 20px;
}

}
.row-dd-relationship .relationship_details_content>label:first-child {
    border-left: 1px solid #eaeaea;
}

.row-dd-relationship .relationship_details_content>label {
    height: 38px;
    background: #f1f1f1;
    padding: 7px 12px;
    font-size: 11px;
    line-height: 23px;
    font-weight: 700;
    margin-bottom: 0;
    border-top: 1px solid #eaeaea;
    border-bottom: 1px solid #eaeaea;
}
</style>

@php
    $relationshipDetails = $relationship['details'];
    $relationshipKeyArray = array_fill_keys(["model", "table", "type", "column", "key", "label", "pivot_table", "pivot", "taggable"], '');
    
    $adv_details = array_diff_key(json_decode(json_encode($relationshipDetails), true), $relationshipKeyArray);
@endphp
<div class="row row-dd row-dd-relationship">
    <div class="col-sm-2">
        <h4 class="field_name"><i class="voyager-heart la la-heartbeat"></i>{{ $relationship->getTranslatedAttribute('display_name') }}</h4>
        <div class="handler flaticon2-sort"></div>
        <strong>{{ __('voyager::database.type') }}:</strong> <span>{{ __('voyager::database.relationship.relationship') }}</span>
        <div class="handler flaticon2-sort"></div>
        <input class="row_order" type="hidden" value="{{ $relationship['order'] }}" name="field_order_{{ $relationship['field'] }}">
    </div>
    <div class="col-sm-2">
        <input type="checkbox" name="field_browse_{{ $relationship['field'] }}" @if(isset($relationship->browse) && $relationship->browse) checked="checked" @elseif(!isset($relationship->browse)) checked="checked" @endif>
        <label for="field_browse_{{ $relationship['field'] }}"> {{ __('voyager::database.relationship.browse') }}</label><br>
        <input type="checkbox" name="field_read_{{ $relationship['field'] }}" @if(isset($relationship->read) && $relationship->read) checked="checked" @elseif(!isset($relationship->read)) checked="checked" @endif>
        <label for="field_read_{{ $relationship['field'] }}"> {{ __('voyager::database.relationship.read') }}</label><br>
        <input type="checkbox" name="field_edit_{{ $relationship['field'] }}" @if(isset($relationship->edit) && $relationship->edit) checked="checked" @elseif(!isset($relationship->edit)) checked="checked" @endif>
        <label for="field_edit_{{ $relationship['field'] }}"> {{ __('voyager::database.relationship.edit') }}</label><br>
        <input type="checkbox" name="field_add_{{ $relationship['field'] }}" @if(isset($relationship->add) && $relationship->add) checked="checked" @elseif(!isset($relationship->add)) checked="checked" @endif>
        <label for="field_add_{{ $relationship['field'] }}"> {{ __('voyager::database.relationship.add') }}</label><br>
        <input type="checkbox" name="field_delete_{{ $relationship['field'] }}" @if(isset($relationship->delete) && $relationship->delete) checked="checked" @elseif(!isset($relationship->delete)) checked="checked" @endif>
        <label for="field_delete_{{ $relationship['field'] }}"> {{ __('voyager::database.relationship.delete') }}</label><br>
    </div>
    <div class="col-sm-2">
        <p>{{ __('voyager::database.relationship.relationship') }}</p>
    </div>
    <div class="col-sm-2">
        @if($isModelTranslatable)
            @include('voyager::multilingual.input-hidden', [
                'isModelTranslatable' => true,
                '_field_name'         => 'field_display_name_' . $relationship['field'],
                '_field_trans' => get_field_translations($relationship, 'display_name')
            ])
        @endif
        <input type="text" name="field_display_name_{{ $relationship['field'] }}" class="form-control relationship_display_name" value="{{ $relationship['display_name'] }}">
    </div>
    <div class="col-sm-4">
        <div class="voyager-relationship-details-btn">
            <i class="voyager-angle-down la la-angle-down"></i><i class="voyager-angle-up la la-angle-up"></i>
            <span class="open_text">{{ __('voyager::database.relationship.open') }}</span>
            <span class="close_text">{{ __('voyager::database.relationship.close') }}</span>
            {{ __('voyager::database.relationship.relationship_details') }}
        </div>
    </div>
    <div class="col-md-12 voyager-relationship-details">
        <a href="{{ route('voyager.bread.delete_relationship', $relationship['id']) }}" class="delete_relationship"><i class="voyager-trash"></i> {{ __('voyager::database.relationship.delete') }}</a>
        <div class="relationship_details_content">
            <p class="relationship_table_select">{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</p>
            <select class="relationship_type select2" name="relationship_type_{{ $relationship['field'] }}">
                <option value="hasOne" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasOne') selected="selected" @endif>{{ __('voyager::database.relationship.has_one') }}</option>
                <option value="hasMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasMany') selected="selected" @endif>{{ __('voyager::database.relationship.has_many') }}</option>
                <option value="belongsTo" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsTo') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to') }}</option>
                <option value="belongsToMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsToMany') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to_many') }}</option>
            </select>
            <select class="relationship_table select2" name="relationship_table_{{ $relationship['field'] }}">
                @foreach($tables as $tablename)
                    <option value="{{ $tablename }}" @if(isset($relationshipDetails->table) && $relationshipDetails->table == $tablename) selected="selected" @endif>{{ ucfirst($tablename) }}</option>
                @endforeach
            </select>
            <span><input type="text" class="form-control" name="relationship_model_{{ $relationship['field'] }}" placeholder="{{ __('voyager::database.relationship.namespace') }}" value="{{ $relationshipDetails->model ?? '' }}"></span>
        </div>
            <div class="relationshipField">
                <div class="relationship_details_content margin_top belongsTo @if($relationshipDetails->type == 'belongsTo') flexed @endif">
                    <label>{{ __('voyager::database.relationship.which_column_from') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span class="label_table_name"></span>?</label>
                    <select name="relationship_column_belongs_to_{{ $relationship['field'] }}" class="new_relationship_field select2">
                        @foreach($fieldOptions as $data)
                            <option value="{{ $data['field'] }}" @if($relationshipDetails->column == $data['field']) selected="selected" @endif>{{ $data['field'] }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="relationship_details_content margin_top hasOneMany @if($relationshipDetails->type == 'hasOne' || $relationshipDetails->type == 'hasMany') flexed @endif">
                    <label>{{ __('voyager::database.relationship.which_column_from') }} <span class="label_table_name"></span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span>?</label>
                    <select name="relationship_column_{{ $relationship['field'] }}" class="new_relationship_field select2 rowDrop" data-table="{{ $relationshipDetails->table ?? '' }}" data-selected="{{ $relationshipDetails->column }}">
                        <option value="{{ $relationshipDetails->column ?? '' }}">{{ $relationshipDetails->column ?? '' }}</option>
                    </select>
                </div>
            </div>
        <div class="relationship_details_content margin_top relationshipPivot @if($relationshipDetails->type == 'belongsToMany') visible @endif">
            <label>{{ __('voyager::database.relationship.pivot_table') }}:</label>
            <select name="relationship_pivot_table_{{ $relationship['field'] }}" class="select2">
                @foreach($tables as $tbl)
                    <option value="{{ $tbl }}" @if(isset($relationshipDetails->pivot_table) && $relationshipDetails->pivot_table == $tbl) selected="selected" @endif>{{ \Illuminate\Support\Str::singular(ucfirst($tbl)) }}</option>
                @endforeach
            </select>
        </div>
        <div class="relationship_details_content margin_top">
            <label>{{ __('voyager::database.relationship.display_the') }} <span class="label_table_name"></span></label>
            <select name="relationship_label_{{ $relationship['field'] }}" class="rowDrop select2" data-table="{{ $relationshipDetails->table ?? '' }}" data-selected="{{ $relationshipDetails->label ?? ''}}">
                <option value="{{ $relationshipDetails->label ?? '' }}">{{ $relationshipDetails->label ?? '' }}</option>
            </select>
            <label class="relationship_key" style="@if($relationshipDetails->type == 'belongsTo' || $relationshipDetails->type == 'belongsToMany') display:block @endif">{{ __('voyager::database.relationship.store_the') }} <span class="label_table_name"></span></label>
            <select name="relationship_key_{{ $relationship['field'] }}" class="rowDrop select2 relationship_key" style="@if($relationshipDetails->type == 'belongsTo' || $relationshipDetails->type == 'belongsToMany') display:block @endif" data-table="@if(isset($relationshipDetails->table)){{ $relationshipDetails->table }}@endif" data-selected="@if(isset($relationshipDetails->key)){{ $relationshipDetails->key }}@endif">
                <option value="{{ $relationshipDetails->key ?? '' }}">{{ $relationshipDetails->key ?? '' }}</option>
            </select>
            <br>
            @isset($relationshipDetails->taggable)
                <label class="relationship_taggable" style="@if($relationshipDetails->type == 'belongsToMany') display:block @endif">
                    {{__('voyager::database.relationship.allow_tagging')}}
                </label>
                <span class="relationship_taggable" style="@if($relationshipDetails->type == 'belongsToMany') display:block @endif">
                    <input 
                        data-switch="true" 
                        type="checkbox" 
                        data-on-color="brand" 
                        name="relationship_taggable_{{ $relationship['field'] }}"
                        data-on-text="{{ __('voyager::generic.yes') }}"
                        data-off-text="{{ __('voyager::generic.no') }}"
                        {{$relationshipDetails->taggable == 'on' ? 'checked' : ''}}>
                </span>
            @endisset
        </div>
        <div class="relationship_details_content margin_top">
            <div class="col-sm-12" style="margin: 0px !important; padding: 0px !important;">
                <div class="alert alert-danger validation-error">
                    {{ __('voyager::json.invalid') }}
                </div>
                <label>{{ __('voyager::database.relationship.relationship_details') }}</label>
                <textarea id="json-input-{{ ($relationship['field']) }}" class="resizable-editor" data-editor="json" name="field_details_{{ $relationship['field'] }}">
                    @if(!empty($adv_details))
                        {{ json_encode($adv_details) }}
                    @else
                        {}
                    @endif
                </textarea>
            </div>
        </div>
    </div>
    <input type="hidden" value="0" name="field_required_{{ $relationship['field'] }}" checked="checked">
    <input type="hidden" name="field_input_type_{{ $relationship['field'] }}" value="relationship">
    <input type="hidden" name="field_{{ $relationship['field'] }}" value="{{ $relationship['field'] }}">
    <input type="hidden" name="relationships[]" value="{{ $relationship['field'] }}">
</div>
