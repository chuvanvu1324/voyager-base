<style>
    .relationship_details {
		margin-bottom: 20px;
		display: flex;
	}

	.relationship_details p {
		float: left;
		padding: 6px 10px;
		background: #fff;
		line-height: 22px;
		margin-right: 0;
		border: 1px solid #eaeaea;
		color: #333;
		margin-bottom: 0;
	}

	p.relationship_table_select {
		background: #EFEFEF;
		color: #76838f;
		font-weight: 700;
		font-size: 12px;
		padding-top: 8px;
	}
	.relationship_details .select2 {
		flex: 1;
		margin-right: 0;
		height: 38px;
	}
	.select2-container--default .select2-selection--single {
		border-radius: 0 !important;
		height: 38px;
	}

	.relationship_details .form-control {
		border-radius: 0;
		height: 38px;
		-webkit-box-flex: 1;
		-ms-flex: 1;
		flex: 1;
	}

	.modal.modal-relationships .relationship_details .belongsTo, 
	.modal.modal-relationships .relationship_details .hasOneMany {
		width: 100%;
		display: none;
	}

	.modal.modal-relationships .relationship_details .belongsTo.flexed, 
	.modal.modal-relationships .relationship_details .hasOneMany.flexed {
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
	}
	.modal.modal-relationships :not(.toggle-group)>label:first-child {
		border-left: 1px solid #eaeaea;
		border-top-left-radius: 3px;
		border-bottom-left-radius: 3px;
	}

	.modal.modal-relationships :not(.toggle-group)>label {
		height: 38px;
		background: #f1f1f1;
		padding: 7px 12px;
		font-size: 11px;
		line-height: 23px;
		font-weight: 700;
		margin-bottom: 0;
		border-top: 1px solid #eaeaea;
		border-bottom: 1px solid #eaeaea;
	}
	.modal.modal-relationships .relationship_details_more {
		position: relative;
		margin-bottom: 0;
	}
	.modal.modal-relationships .relationship_details_more .well {
		position: relative;
		padding-top: 55px;
		margin-bottom: 5px;
		background: 0 0;
		border: 1px solid #f1f1f1;
	}
	.well {
		padding: 20px;
		box-shadow: inset 0 0 1px rgb(0 0 0 / 2%);
	}
	.well {
		min-height: 20px;
		margin-bottom: 20px;
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
	}
	.modal.modal-relationships .relationship_details_more :not(.toggle-group)>label {
		height: 38px;
		background: #f1f1f1;
		padding: 7px 12px;
		font-size: 11px;
		line-height: 23px;
		font-weight: 700;
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		border: 0;
	}
</style>

<form role="form" action="{{ route('voyager.bread.relationship') }}" method="POST">
	<div class="modal fade modal-relationships" id="new_relationship_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="la la-heartbeat"></i> {{ \Illuminate\Support\Str::singular(ucfirst($table)) }}
						{{ __('voyager::database.relationship.relationships') }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
			        	@if(isset($dataType->id))
				            <div class="col-md-12 relationship_details">
				                <p class="relationship_table_select">{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</p>
				                <select class="relationship_type select2" name="relationship_type">
				                    <option value="hasOne" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasOne') selected="selected" @endif>{{ __('voyager::database.relationship.has_one') }}</option>
				                    <option value="hasMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasMany') selected="selected" @endif>{{ __('voyager::database.relationship.has_many') }}</option>
				                    <option value="belongsTo" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsTo') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to') }}</option>
				                    <option value="belongsToMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsToMany') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to_many') }}</option>
				                </select>
				                <select class="relationship_table select2" name="relationship_table">
				                    @foreach($tables as $tbl)
				                        <option value="{{ $tbl }}" @if(isset($relationshipDetails->table) && $relationshipDetails->table == $tbl) selected="selected" @endif>{{ \Illuminate\Support\Str::singular(ucfirst($tbl)) }}</option>
				                    @endforeach
				                </select>
				                <span><input type="text" class="form-control" name="relationship_model" placeholder="{{ __('voyager::database.relationship.namespace') }}" value="{{ $relationshipDetails->model ?? ''}}"></span>
				            </div>
				            <div class="col-md-12 relationship_details relationshipField">
				            	<div class="belongsTo">
				            		<label>{{ __('voyager::database.relationship.which_column_from') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span class="label_table_name"></span>?</label>
				            		<select name="relationship_column_belongs_to" class="new_relationship_field select2">
				                    	@foreach($fieldOptions as $data)
				                        	<option value="{{ $data['field'] }}">{{ $data['field'] }}</option>
				                    	@endforeach
				               		</select>
				               	</div>
				               	<div class="hasOneMany flexed">
				            		<label>{{ __('voyager::database.relationship.which_column_from') }} <span class="label_table_name"></span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span>?</label>
					                <select name="relationship_column" class="new_relationship_field select2 rowDrop" data-table="{{ $tables[0] }}" data-selected="">
					                </select>
					            </div>
				            </div>
				            <div class="col-md-12 relationship_details relationshipPivot">
				            	<label>{{ __('voyager::database.relationship.pivot_table') }}:</label>
				            	<select name="relationship_pivot" class="select2">
		                        	@foreach($tables as $tbl)
				                        <option value="{{ $tbl }}" @if(isset($relationshipDetails->table) && $relationshipDetails->table == $tbl) selected="selected" @endif>{{ \Illuminate\Support\Str::singular(ucfirst($tbl)) }}</option>
				                    @endforeach
		                        </select>
				            </div>
				            <div class="col-md-12 relationship_details_more">
				                <div class="well">
				                    <label>{{ __('voyager::database.relationship.selection_details') }}</label>
				                    <p><strong>{{ __('voyager::database.relationship.display_the') }} <span class="label_table_name"></span>: </strong>
				                        <select name="relationship_label" class="rowDrop select2" data-table="{{ $tables[0] }}" data-selected="" style="width: 100%">
				                        </select>
				                    </p>
				                    <p class="relationship_key"><strong>{{ __('voyager::database.relationship.store_the') }} <span class="label_table_name"></span>: </strong>
				                        <select name="relationship_key" class="rowDrop select2" data-table="{{ $tables[0] }}" data-selected="" style="width: 100%">
				                        </select>
									</p>

									<p class="relationship_taggable"><strong>{{ __('voyager::database.relationship.allow_tagging') }}:</strong> <br>
										{{-- <input type="checkbox" name="relationship_taggable" class="toggleswitch" data-on="{{ __('voyager::generic.yes') }}" data-off="{{ __('voyager::generic.no') }}"> --}}
										<input 
                                                data-switch="true" 
                                                type="checkbox" 
                                                data-on-color="brand" 
                                                name="relationship_taggable"
                                                data-on-text="{{ __('voyager::generic.yes') }}"
                                                data-off-text="{{ __('voyager::generic.no') }}">
				                    </p>
				                </div>
							</div>
				        @else
				        	<div class="col-md-12">
				        		<h5><i class="voyager-rum-1"></i> {{ __('voyager::database.relationship.easy_there') }}</h5>
				        		<p class="relationship-warn">{!! __('voyager::database.relationship.before_create') !!}</p>
				        	</div>
				        @endif

			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('voyager::database.relationship.cancel') }}</button>
					@if(isset($dataType->id))
						<button class="btn btn-danger btn-relationship">{{ __('voyager::database.relationship.add_new') }}</button>
					@endif
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="{{ $dataType->id ?? '' }}" name="data_type_id">
	{{ csrf_field() }}
</form>

