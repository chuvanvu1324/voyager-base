@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))
@section('css')
    <link href="{{ metoger_asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ metoger_asset('css/pages/bread/browse.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_header')
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">{{ $dataType->getTranslatedAttribute('display_name_plural') }}</h3>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    @can('add', app($dataType->model_name))
                        <a href="{{ route('voyager.' . $dataType->slug . '.create') }}">
                            <button type="button" class="btn btn-success btn-elevate-hover btn-pill">
                                <i class="flaticon-plus"></i> {{ __('voyager::generic.add_new') }}
                            </button>&nbsp;
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('voyager::menus.partial.notice')
        @include('voyager::alerts')
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <table id="kt_dataTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        @foreach($dataType->browseRows as $row)
                                        <th>{{ $row->display_name }}</th>
                                        @endforeach
                                        <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dataTypeContent as $data)
                                        <tr>
                                            @foreach($dataType->browseRows as $row)
                                            <td>
                                                @if($row->type == 'image')
                                                    <img src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @else
                                                    {{ $data->{$row->field} }}
                                                @endif
                                            </td>
                                            @endforeach
                                            <td class="no-sort no-click bread-actions">
                                                @can('delete', $data)
                                                    <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $data->{$data->getKeyName()} }}">
                                                        <i class="flaticon-delete"></i> {{ __('voyager::generic.delete') }}
                                                    </div>
                                                @endcan
                                                @can('edit', $data)
                                                    <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->{$data->getKeyName()}) }}" class="btn btn-sm btn-primary pull-right edit">
                                                        <i class="fa fa-edit"></i> {{ __('voyager::generic.edit') }}
                                                    </a>
                                                @endcan
                                                @can('edit', $data)
                                                    <a href="{{ route('voyager.'.$dataType->slug.'.builder', $data->{$data->getKeyName()}) }}" class="btn btn-sm btn-success pull-right">
                                                        <i class="flaticon-eye"></i> {{ __('voyager::generic.builder') }}
                                                    </a>
                                                @endcan
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content panel-warning">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }}
                        {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h5>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right"
                        data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit"
                            class="btn btn-danger pull-right delete-confirm">{{ __('voyager::generic.delete_confirm') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ metoger_asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#kt_dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });

        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__menu']) }}'.replace('__menu', $(this).data('id'));

            $('#delete_modal').modal('show');
        });
    </script>
@stop
