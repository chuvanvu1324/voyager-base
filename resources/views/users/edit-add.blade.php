@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .kt-avatar .kt-avatar__holder {
            width: 220px;
            height: 220px;
        }
    </style>
@stop

@section('page_header')
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">{{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}</h3>
            </div>
        </div>
    </div>
@stop

@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobiless">
        <div class="kt-portlet__body">
            <form class="form-edit-add" role="form"
                action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                method="POST" enctype="multipart/form-data" autocomplete="off">
                <!-- PUT Method if we are editing -->
                @if(isset($dataTypeContent->id))
                    {{ method_field("PUT") }}
                @endif
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-bordered">
                        {{-- <div class="panel"> --}}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="name">{{ __('voyager::generic.name') }}</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('voyager::generic.name') }}"
                                        value="{{ old('name', $dataTypeContent->name ?? '') }}">
                                </div>

                                <div class="form-group">
                                    <label for="email">{{ __('voyager::generic.email') }}</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="{{ __('voyager::generic.email') }}"
                                        value="{{ old('email', $dataTypeContent->email ?? '') }}">
                                </div>

                                <div class="form-group">
                                    <label for="password">{{ __('voyager::generic.password') }}</label>
                                    @if(isset($dataTypeContent->password))
                                        <br>
                                        <small>{{ __('voyager::profile.password_hint') }}</small>
                                    @endif
                                    <input type="password" class="form-control" id="password" name="password" value="" autocomplete="new-password">
                                </div>

                                @can('editRoles', $dataTypeContent)
                                    <div class="form-group">
                                        <label for="default_role">{{ __('voyager::profile.role_default') }}</label>
                                        @php
                                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                                            $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                                            $options = $row->details;
                                        @endphp
                                        @include('voyager::formfields.relationship')
                                    </div>
                                    <div class="form-group">
                                        <label for="additional_roles">{{ __('voyager::profile.roles_additional') }}</label>
                                        @php
                                            $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                                            $options = $row->details;
                                        @endphp
                                        @include('voyager::formfields.relationship')
                                    </div>
                                @endcan
                                @php
                                if (isset($dataTypeContent->locale)) {
                                    $selected_locale = $dataTypeContent->locale;
                                } else {
                                    $selected_locale = config('app.locale', 'en');
                                }

                                @endphp
                                <div class="form-group">
                                    <label for="locale">{{ __('voyager::generic.locale') }}</label>
                                    <select class="form-control select2" id="locale" name="locale">
                                        @foreach (Voyager::getLocales() as $locale)
                                        <option value="{{ $locale }}"
                                        {{ ($locale == $selected_locale ? 'selected' : '') }}>{{ $locale }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar_1">
                            <div class="kt-avatar__holder" style="background-image: url({{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }})"></div>
                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                <i class="fa fa-pen"></i>
                                <input type="file"  data-name="avatar" name="avatar">
                            </label>
                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                <i class="fa fa-times"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary pull-right save">
                    {{ __('voyager::generic.save') }}
                </button>
            </form>

            <iframe id="form_target" name="form_target" style="display:none"></iframe>
            <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
                {{ csrf_field() }}
                <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
            </form>

        </div>
    </div>
</div>
@stop

@section('javascript')
    <script src="{{ metoger_asset('js/pages/crud/file-upload/ktavatar.js') }}" type="text/javascript"></script>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
