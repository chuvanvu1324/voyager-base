<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<!-- begin::Head -->

<head>
	<meta charset="utf-8" />
	<title>@yield('title', 'Admin - '.Voyager::setting("admin.title"))</title>
	<meta name="description" content="Login page example">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet"
		href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <link rel="stylesheet" href="{{ metoger_asset('plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/style.bundle.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/header/base/light.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/header/menu/light.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/brand/dark.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/aside/dark.css') }}">

	<link href="{{ metoger_asset('css/pages/login/login-2.css') }}" rel="stylesheet" type="text/css" />




	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	@yield('pre_css')
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
	class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
	@yield('content')
	<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
</script>
<!-- end::Global Config -->
<script type="text/javascript" src="{{ metoger_asset('plugins/global/plugins.bundle.js') }}"></script>
<script type="text/javascript" src="{{ metoger_asset('js/scripts.bundle.js') }}"></script>


<!--begin::Page Scripts(used by this page) -->
<script src="{{ metoger_asset('js/pages/custom/login/login-general.js') }}" type="text/javascript"></script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>