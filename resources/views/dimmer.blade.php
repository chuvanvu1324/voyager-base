<div class="kt-portlet kt-portlet--height-fluid kt-widget19">
    <div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
        <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url({{ $image }})">
            <div class="kt-widget19__title kt-font-light">
                <h3>{!! $title !!}</h3>
                <p>{!! $text !!}</p>
            </div>
            @if (isset($icon))
            <div class="icon-widget">
                <i class="{{$icon}}"></i>
            </div>
            @endif
            <div class="kt-widget19__shadow"></div>
            <div class="kt-widget19__labels">
                <a href="{{ $button['link'] }}" class="btn btn-label-light-o2 btn-bold">{!! $button['text'] !!}</a>
            </div>
        </div>
    </div>
</div>