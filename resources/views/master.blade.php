<?php
if (\Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'http://') ||
\Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'https://')) {
$user_avatar = Auth::user()->avatar;
} else {
$user_avatar = Voyager::image(Auth::user()->avatar);
} ?>
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8" />
    <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="assets-path" content="{{ route('voyager.voyager_assets') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!-- Google Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!-- Favicon -->
    <?php $admin_favicon = Voyager::setting('admin.icon_image', ''); ?>
    @if($admin_favicon == '')
        <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/png">
    @else
        <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">
    @endif
    @yield('page_css')
    @yield('css')
    @stack('css')
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ metoger_asset('plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/style.bundle.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/header/base/light.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/header/menu/light.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/brand/dark.css') }}">
    <link rel="stylesheet" href="{{ metoger_asset('css/skins/aside/dark.css') }}">

    <!--begin::Page Vendors Styles(used by this page) -->


<style>
    @keyframes spin {
        0% {
            -webkit-transform: rotate(0);
            transform: rotate(0);
        }
        100% {
            -webkit-transform: rotate(359deg);
            transform: rotate(359deg);
        }
    }
    #voyager-loader {
        /* background: #f9f9f991; */
        position: fixed;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        z-index: 99;
    }
    #voyager-loader img {
        width: 100%;
        height: 100%;
        -webkit-animation: spin 1s linear infinite;
        animation: spin 1s linear infinite;
    }
    .tranxy {
        width: 100px;
        height: 100px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .alert p {
        margin-bottom: 0;
    }
</style>

</head>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed {{ trim($__env->yieldContent('page_header')) ? 'kt-subheader--enabled kt-subheader--fixed kt-subheader--solid' : '' }} kt-aside--enabled kt-aside--fixed">
    <div id="voyager-loader">
        <div class="tranxy">
            <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
            @if($admin_loader_img == '')
                <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
            @else
                <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
            @endif
        </div>
    </div>
    <!-- begin:: Page -->

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="{{ route('voyager.dashboard') }}">
                <img alt="Logo" src="{{ metoger_asset('media/logos/logo-light.png') }}" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left"
                id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i></button>
        </div>
    </div>

    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <!-- begin:: Aside -->
            @include('voyager::dashboard.sidebar')
            <!-- end:: Aside -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                @include('voyager::dashboard.navbar')
                <!-- end:: Header -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                    <!-- begin:: Subheader -->
                    @yield('page_header')
                    <!-- end:: Subheader -->

                    <!-- begin:: Content -->
                    @yield('content')
                    <!-- end:: Content -->
                </div>

                <!-- begin:: Footer -->
                @include('voyager::partials.app-footer')
                <!-- end:: Footer -->
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    <!-- end::Scrolltop -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };

    </script>

    <script type="text/javascript" src="{{ metoger_asset('plugins/global/plugins.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ metoger_asset('js/scripts.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>

    <script>
        @if(Session::has('alerts'))
            let alerts = {!! json_encode(Session::get('alerts')) !!};
            helpers.displayAlerts(alerts, toastr);
        @endif
    
        @if(Session::has('message'))
    
        // TODO: change Controllers to use AlertsMessages trait... then remove this
        var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
        var alertMessage = {!! json_encode(Session::get('message')) !!};
        var alerter = toastr[alertType];
    
        if (alerter) {
            alerter(alertMessage);
        } else {
            toastr.error("toastr alert-type " + alertType + " is unknown");
        }
        @endif
    </script>
    @include('voyager::media.manager')
    @yield('javascript')
    @stack('javascript')
    @if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
        @foreach(config('voyager.additional_js') as $js)<script type="text/javascript" src="{{ asset($js) }}"></script>@endforeach
    @endif
    

    <script>
        (function(){
                // var appContainer = document.querySelector('.app-container'),
                //     sidebar = appContainer.querySelector('.side-menu'),
                //     navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                    var loader = document.getElementById('voyager-loader')
                    // hamburgerMenu = document.querySelector('.hamburger'),
                    // sidebarTransition = sidebar.style.transition,
                    // navbarTransition = navbar.style.transition,
                    // containerTransition = appContainer.style.transition;

                // sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                // appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                // navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                if (window.innerWidth > 768 && window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                    // appContainer.className += ' expanded no-animation';
                    loader.style.left = (sidebar.clientWidth/2)+'px';
                    // hamburgerMenu.className += ' is-active no-animation';
                }

            //    navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
            //    sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
            //    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
        })();
    </script>
    
</body>



</html>
